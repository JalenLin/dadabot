require 'cinch'

class DadaSpeech
	@@speech_text = Array.new
	@@notDadaList = Array.new
	
	def updateNotDada
		@@notDadaList.clear
        File.open("notDada.txt", "r") do |line|
            while(fline = line.gets)
                @@notDadaList.push(fline.chomp)
            end
        end
	end
	
	def updateSpeech
		@@speech_text.clear
		File.open("speech.txt", "r") do |line|
			while(fline = line.gets)
				@@speech_text.push(fline.chomp)
			end
		end
	end

	def addSpeech(add_text)
		add_speech = File.new("speech.txt", "a")
		puts add_text
		add_speech.puts(add_text)
		add_speech.close
		updateSpeech
	end

	def getSpeech
		return @@speech_text
	end

	def getNotDadaList
		return @@notDadaList
	end
end

bot = Cinch::Bot.new do

	rate = 1
	rate_time = 3600

  configure do |c|
    c.server = "irc.freenode.org"
    c.channels = ["#csSula LINPAO"]
	c.nick = "You_Dada_bot"
  end
	
  user_time = Hash.new
  dada = DadaSpeech.new
    
  	on :connect do |m|
		dada.updateSpeech
		dada.updateNotDada
	end
    on :message do |m|
		if !dada.getNotDadaList.include?(m.user.to_s)
			if (user_time.has_key?(m.user) && Time.now - user_time[m.user] > rate_time) || (user_time.has_key?(m.user) && 100 - Random::rand(100) <= rate )
				m.reply dada.getSpeech[Random::rand(dada.getSpeech.length)].gsub('$1', m.user.to_s)
				user_time[m.user] = Time.now
			elsif !user_time.has_key?(m.user)
				m.reply dada.getSpeech[Random::rand(dada.getSpeech.length)].gsub('$1', m.user.to_s)
				user_time[m.user] = Time.now
			else
				user_time[m.user] = Time.now
			end
		end
    end

	on :message, '@notdada' do |m|
		dada.updateNotDada
		m.reply dada.getNotDadaList.to_s
	end

	on :message, '@help' do |m|
		m.reply '可以使用 "@addDadaSpeech 台詞" 加入拜大大台詞，其中可用$1表示人名'
		m.reply 'Example: @addDadaSpeech $1是大大'
	end

	on :message, /@addDadaSpeech .*/ do |m|
		dada.addSpeech(m.message.to_s.gsub('@addDadaSpeech ', ""))
		dada.updateSpeech
		m.reply "'#{m.message.to_s.gsub('@addDadaSpeech ', "")}' 已加入"
	end

	on :message, '@updateSpeech' do |m|
		dada.updateSpeech
		m.reply dada.getSpeech.to_s
	end

	on :message, '@getRate' do |m|
		m.reply "目前大大觸發機率為 #{rate}%"
	end
	
	on :message, /@setRate \d+/ do |m|
		if (m.message.delete('@setRate ').to_i <= 100) || (m.message.delete('@setRate ').to_i >= 0)
			rate = m.message.delete('@setRate ').to_i
			m.reply "目前大大觸發機率為 #{rate}%"
		end
	end

	on :message, '@getRateTime' do |m|
		m.reply "目前大大觸發機率為 #{rate_time}秒"
	end
	
	on :message, /@setRateTime \d+/ do |m|
		rate_time = m.message.delete('@setRateTime ').to_i
		m.reply "目前大大觸發機率為 #{rate_time}秒"
	end
end

bot.start
